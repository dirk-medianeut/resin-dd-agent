# Resin Datadog Agent

**Usage**:

Intel nuc (Debian):
```
make build
```

Intel nuc (Alpine):
```
OS=alpine make build
```

Raspberry PI (Debian)*:
```
RESIN_MACHINE_NAME=raspberrypi3 DOCKER=docker-arm make build
```

Raspberry PI (Alpine)*:
```
OS=alpine RESIN_MACHINE_NAME=raspberrypi3 DOCKER=docker-arm make build
```

_View the Makefile for more info_


*: `docker-arm` is an expected alias to a custom (remote?) docker engine which runs on ARM
