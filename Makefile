ROOT_DIR := $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

OS ?= debian
RESIN_MACHINE_NAME ?= intel-nuc
DOCKER ?= docker

.PHONY: dockerfile
dockerfile:
	@cp ${ROOT_DIR}/${OS}/Dockerfile.template ${ROOT_DIR}.Dockerfile.${RESIN_MACHINE_NAME}.${OS}
	@sed -i "s/%%RESIN_MACHINE_NAME%%/${RESIN_MACHINE_NAME}/g" ${ROOT_DIR}.Dockerfile.${RESIN_MACHINE_NAME}.${OS}
	@echo "Created Dockerfile"

.PHONY: build
build: dockerfile
	${DOCKER} build -f ${ROOT_DIR}.Dockerfile.${RESIN_MACHINE_NAME}.${OS} -t medianeut/resin-dd-agent:latest-${RESIN_MACHINE_NAME}-${OS} ${ROOT_DIR}

.PHONY: push
push: build
	${DOCKER} push medianeut/resin-dd-agent:latest-${RESIN_MACHINE_NAME}-${OS}
