#!/bin/bash

DD_CONF_DIR=${DD_CONF_DIR:-/conf.d}
[ -d $DD_CONF_DIR ] || mkdir -p $DD_CONF_DIR

DD_CHECKS_DIR=${DD_CHECKS_DIR:-/checks.d}
[ -d $DD_CHECKS_DIR ] || mkdir -p $DD_CHECKS_DIR

# Copy the custom checks and confs in the /etc/datadog-agent folder
find ${DD_CONF_DIR} -name '*.yaml' -exec cp --parents -fv {} /etc/datadog-agent/ \;
find ${DD_CHECKS_DIR} -name '*.py' -exec cp --parents -fv {} /etc/datadog-agent/ \;
